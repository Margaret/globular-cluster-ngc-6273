# CAPOS The bulge Cluster APOgee Survey IV Elemental Abundances of the intriguing Metal Poor Bulge Globular Cluster NGC 6273

NGC 6273 is an intriguing metal-poor ([Fe/H] ~ -1.74) globular cluster (GC) located toward the dense regions of the Milky Way (MW) galaxy, where the source crowding is extremely high. 